package com.adisorin.drivingschool;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper {
 
	// answer separator to be used in case a question has more than one answer
  public static final String ANSWER_SEPARATOR = ",";
  public static final String ANSWER_A = "A";
  public static final String ANSWER_B = "B";
  public static final String ANSWER_C = "C";
  public static final String TABLE_NAME = "drivingTestQuestions";
  public static final String COLUMN_NAME_ID = "id";
  public static final String COLUMN_NAME_QUESTION = "question";
  public static final String COLUMN_NAME_CATEGORY = "category";
  public static final String COLUMN_NAME_IMAGE = "image";
  public static final String COLUMN_NAME_ANSWER_A = "answerA";
  public static final String COLUMN_NAME_ANSWER_B = "answerB";
  public static final String COLUMN_NAME_ANSWER_C = "answerC";
  public static final String COLUMN_NAME_CORRECT_ANSWER = "correctAnswer";
  private static final int DATABASE_VERSION = 1;
  private static final String DATABASE_NAME = "drivingTest";
  
  public static final String CATEGORY = "category";
  public static final int CATEGORY_INDICATOARE_MARCAJE = 0;
  public static final int CATEGORY_SEMNALELE_POLITISTULUI = 1;
  public static final int CATEGORY_SEMNALE_LUMINOASE = 2;
  public static final int CATEGORY_DEPASIREA = 3;
  public static final int CATEGORY_MANEVRE_IN_TIMPUL_MERSULUI = 4;
  public static final int CATEGORY_OPRIRE_STATIONARE_PARCARE = 5;
  public static final int CATEGORY_TRECEREA_LA_NIVEL_CU_CALEA_FERATA = 6;
  public static final int CATEGORY_CIRCULATIA_PE_AUTOSTRAZI = 7;
  public static final int CATEGORY_MECANICA = 8;
  public static final int CATEGORY_SANCTIUNI_INFRACTIUNI = 9;
  public static final int CATEGORY_CONDUCERE_PREVENTIVA = 10;
  
  public DBHelper(Context context) {
    super(context, DATABASE_NAME, null, DATABASE_VERSION);
  }
  
  @Override
  public void onCreate(SQLiteDatabase db) {
	  final String SQL_CREATE_TABLE = 
		      "CREATE TABLE " + TABLE_NAME + " (" + 
		          COLUMN_NAME_ID + 							" INTEGER PRIMARY KEY AUTOINCREMENT, " + 
		         COLUMN_NAME_QUESTION + 				" VARCHAR(800), " +
		         COLUMN_NAME_CATEGORY + 				" INTEGER, " +
		         COLUMN_NAME_IMAGE        + 				" INTEGER, " + 
		         COLUMN_NAME_ANSWER_A +	 			" VARCHAR(800), " +
		         COLUMN_NAME_ANSWER_B + 				" VARCHAR(800), " +
		         COLUMN_NAME_ANSWER_C + 				" VARCHAR(800), " +
		         COLUMN_NAME_CORRECT_ANSWER +  " VARCHAR(5) " +
		       "); ";
    db.execSQL(SQL_CREATE_TABLE);
    insertMassiveData(db);
    
  }
  
  @Override
  public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    
  }
   
  private void insertMassiveData(SQLiteDatabase db) {
		ContentValues contentValues = new ContentValues();
		
		// q818 - A
		contentValues.put(DBHelper.COLUMN_NAME_QUESTION, "Indicatorul va obliga:");
		contentValues.put(DBHelper.COLUMN_NAME_CATEGORY, DBHelper.CATEGORY_INDICATOARE_MARCAJE);
		contentValues.put(DBHelper.COLUMN_NAME_IMAGE, R.drawable.q1c0);
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_A, "A. Sa circulati inainte sau la dreapta in prima intersectie");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_B, "B. Sa virati la stanga, deoarece accesul in celelalte directii este interzis");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_C, "C. Sa urmati traseul ocolitor, presemnalizat la dreapta");
		contentValues.put(DBHelper.COLUMN_NAME_CORRECT_ANSWER, "A");

		db.insert(DBHelper.TABLE_NAME, null, contentValues);

		// q850 - A
		contentValues = new ContentValues();

		contentValues.put(DBHelper.COLUMN_NAME_QUESTION, "Cum procedati la intalnirea panoului si a indicatorului:");
		contentValues.put(DBHelper.COLUMN_NAME_CATEGORY, DBHelper.CATEGORY_INDICATOARE_MARCAJE);
		contentValues.put(DBHelper.COLUMN_NAME_IMAGE, R.drawable.q2c0);
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_A, "A. Opriti, va asigurati si acordati prioritate tuturor vehiculelor");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_B, "B. Acordati prioritate vehiculelor care circula din stanga");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_C, "C. Acordati prioritate numai vehiculelor de transport public de persoane");
		contentValues.put(DBHelper.COLUMN_NAME_CORRECT_ANSWER, "A");

		db.insert(DBHelper.TABLE_NAME, null, contentValues);

		// q854 - A
		contentValues = new ContentValues();

		contentValues.put(DBHelper.COLUMN_NAME_QUESTION, "Puteti trece cu autovehiculul peste o linie continua daca:");
		contentValues.put(DBHelper.COLUMN_NAME_CATEGORY, DBHelper.CATEGORY_INDICATOARE_MARCAJE);
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_A, "A. Linia continua este insotita de o linie discontinua, iar aceasta din urma este cea mai aproape de sensul dvs. de deplasare");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_B, "B. Aceasta este aplicata pe drumuri cu mai mult de o banda pe sensul de mers");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_C, "C. Aceasta separa sensurile de circulatie");
		contentValues.put(DBHelper.COLUMN_NAME_CORRECT_ANSWER, "A");

		db.insert(DBHelper.TABLE_NAME, null, contentValues);

		// q855 - A
		contentValues = new ContentValues();

		contentValues.put(DBHelper.COLUMN_NAME_QUESTION, "Marcajul aplicat pe marginea din dreapta a partii carosabile, format dintr-o linie de zig-zag, semnifica:");
		contentValues.put(DBHelper.COLUMN_NAME_CATEGORY, DBHelper.CATEGORY_INDICATOARE_MARCAJE);
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_A, "A. Interzicerea stationarii");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_B, "B. Interzicerea opririi");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_C, "C. Alte pericole");
		contentValues.put(DBHelper.COLUMN_NAME_CORRECT_ANSWER, "A");

		db.insert(DBHelper.TABLE_NAME, null, contentValues);

		// q817 - A
		contentValues = new ContentValues();

		contentValues.put(DBHelper.COLUMN_NAME_QUESTION, "Indicatorul nu permite schimarea directiei de mers spre stanga:");
		contentValues.put(DBHelper.COLUMN_NAME_IMAGE, R.drawable.q3c0);
		contentValues.put(DBHelper.COLUMN_NAME_CATEGORY, DBHelper.CATEGORY_INDICATOARE_MARCAJE);
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_A, "A. In intersectia inaintea careia este instalat");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_B, "B. In toate intersectiile, pana la aparitia indicatorului 'Sfarsitul tuturor restrictiilor'");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_C, "C. La 200 m de locul in care este instalat acesta");
		contentValues.put(DBHelper.COLUMN_NAME_CORRECT_ANSWER, "A");

		db.insert(DBHelper.TABLE_NAME, null, contentValues);

		// q867 - A
		contentValues = new ContentValues();

		contentValues.put(DBHelper.COLUMN_NAME_QUESTION, "Care dintre cele doua indicatoare obliga la schimbarea directiei de mers spre dreapta, dupa depasirea locului in care este instalat?");
		contentValues.put(DBHelper.COLUMN_NAME_IMAGE, R.drawable.q4c0);
		contentValues.put(DBHelper.COLUMN_NAME_CATEGORY, DBHelper.CATEGORY_INDICATOARE_MARCAJE);
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_A, "A. Indicatorul 2");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_B, "B. Indicatorul 1");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_C, "C. Ambele indicatoare");
		contentValues.put(DBHelper.COLUMN_NAME_CORRECT_ANSWER, "A");

		db.insert(DBHelper.TABLE_NAME, null, contentValues);

		// q843 - C
		contentValues = new ContentValues();

		contentValues.put(DBHelper.COLUMN_NAME_QUESTION, "Indicatorul precizeaza");
		contentValues.put(DBHelper.COLUMN_NAME_IMAGE, R.drawable.q5c0);
		contentValues.put(DBHelper.COLUMN_NAME_CATEGORY, DBHelper.CATEGORY_INDICATOARE_MARCAJE);
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_A, "A. Pe banda din stanga, vehiculele vor circula cu cel mult viteza indicata");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_B, "B. Pe banda din dreapta, se poate circula fara restrictie de viteza");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_C, "C. Pe banda din stanga, pot circula cei care se deplaseaza cel putin cu viteza mentionata");
		contentValues.put(DBHelper.COLUMN_NAME_CORRECT_ANSWER, "C");

		db.insert(DBHelper.TABLE_NAME, null, contentValues);

		// q842 - A
		contentValues = new ContentValues();

		contentValues.put(DBHelper.COLUMN_NAME_QUESTION, "Ce obligatii aveti la intalnirea indicatorului:");
		contentValues.put(DBHelper.COLUMN_NAME_IMAGE, R.drawable.q6c0);
		contentValues.put(DBHelper.COLUMN_NAME_CATEGORY, DBHelper.CATEGORY_INDICATOARE_MARCAJE);
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_A, "A. Opriti autovehiculul, fara a depasi indicatorul, va asigurati, apoi traversati");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_B, "B. Traversati calea ferata cu viteza redusa, dupa ce v-ati asigurat ca nu se apropie trenul");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_C, "C. Nicio obligatie, indicatorul nu vi se adreseaza");
		contentValues.put(DBHelper.COLUMN_NAME_CORRECT_ANSWER, "A");

		db.insert(DBHelper.TABLE_NAME, null, contentValues);

		// q819 - A
		contentValues = new ContentValues();

		contentValues.put(DBHelper.COLUMN_NAME_QUESTION, "Acest indicator va avertizeaza ca urmeaza:");
		contentValues.put(DBHelper.COLUMN_NAME_IMAGE, R.drawable.q7c0);
		contentValues.put(DBHelper.COLUMN_NAME_CATEGORY, DBHelper.CATEGORY_INDICATOARE_MARCAJE);
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_A, "A. Alte pericole");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_B, "B. Un drum fara marcaje");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_C, "C. Un drum cu marcaje");
		contentValues.put(DBHelper.COLUMN_NAME_CORRECT_ANSWER, "A");

		db.insert(DBHelper.TABLE_NAME, null, contentValues);

		// q866 - C
		contentValues = new ContentValues();

		contentValues.put(DBHelper.COLUMN_NAME_QUESTION, "Sfarsitul zonei in care oprirea a fost interzisa este anuntata de:");
		contentValues.put(DBHelper.COLUMN_NAME_IMAGE, R.drawable.q8c0);
		contentValues.put(DBHelper.COLUMN_NAME_CATEGORY, DBHelper.CATEGORY_INDICATOARE_MARCAJE);
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_A, "A. Ambele indicatoare");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_B, "B. Indicatorul 1");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_C, "C. Indicatorul 2, insotit de panoul aditional");
		contentValues.put(DBHelper.COLUMN_NAME_CORRECT_ANSWER, "C");

		db.insert(DBHelper.TABLE_NAME, null, contentValues);

		// q841 - A
		contentValues = new ContentValues();

		contentValues.put(DBHelper.COLUMN_NAME_QUESTION, "Ce obligatii aveti la intalnirea indicatorului:");
		contentValues.put(DBHelper.COLUMN_NAME_IMAGE, R.drawable.q9c0);
		contentValues.put(DBHelper.COLUMN_NAME_CATEGORY, DBHelper.CATEGORY_INDICATOARE_MARCAJE);
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_A, "A. Sa nu depasiti viteza de 50 km/h, deoarece urmeaza o curba deosebit de periculoasa spre dreapta");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_B, "B. Sa reduceti viteza pana la 40 km/h si sa circulati cu faza de intalnire");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_C, "C. Sa nu depasiti vehiculele de transport de marfuri");
		contentValues.put(DBHelper.COLUMN_NAME_CORRECT_ANSWER, "A");

		db.insert(DBHelper.TABLE_NAME, null, contentValues);

		// q796 - A
		contentValues = new ContentValues();

		contentValues.put(DBHelper.COLUMN_NAME_QUESTION, "Va este permisa trecerea peste marcajul longitudinal din imagine?");
		contentValues.put(DBHelper.COLUMN_NAME_IMAGE, R.drawable.q10c0);
		contentValues.put(DBHelper.COLUMN_NAME_CATEGORY, DBHelper.CATEGORY_INDICATOARE_MARCAJE);
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_A, "A. Da, deoarece linia discontinua este cea mai apropiata de vehiculul pe care il conduceti");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_B, "B. Nu, deoarece este un marcaj dublu");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_C, "C. Nu, deoarece in acest caz depasirea este interzisa");
		contentValues.put(DBHelper.COLUMN_NAME_CORRECT_ANSWER, "A");

		db.insert(DBHelper.TABLE_NAME, null, contentValues);

		// q824 - A
		contentValues = new ContentValues();

		contentValues.put(DBHelper.COLUMN_NAME_QUESTION, "La intalnirea carui indicator conducatorul auto nu are prioritate de trecere?");
		contentValues.put(DBHelper.COLUMN_NAME_CATEGORY, DBHelper.CATEGORY_INDICATOARE_MARCAJE);
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_A, "A. Prioritate pentru circulatia din sens invers");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_B, "B. Prioritate fata de circulatia din sens invers");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_C, "C. Drum cu prioritate");
		contentValues.put(DBHelper.COLUMN_NAME_CORRECT_ANSWER, "A");

		db.insert(DBHelper.TABLE_NAME, null, contentValues);

		// q816 - A
		contentValues = new ContentValues();

		contentValues.put(DBHelper.COLUMN_NAME_QUESTION, "La intalnirea indicatorului aveti obligatia:");
		contentValues.put(DBHelper.COLUMN_NAME_CATEGORY, DBHelper.CATEGORY_INDICATOARE_MARCAJE);
		contentValues.put(DBHelper.COLUMN_NAME_IMAGE, R.drawable.q11c0);
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_A, "A. De a circula cu atentie, deoarece urmeaza o curba la stanga");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_B, "B. De a schimba obligatoriu directia de mers spre stanga");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_C, "C. De a reduce viteza, deoarece urmeaza o intersectie periculoasa");
		contentValues.put(DBHelper.COLUMN_NAME_CORRECT_ANSWER, "A");

		db.insert(DBHelper.TABLE_NAME, null, contentValues);

		// q852 - B
		contentValues = new ContentValues();

		contentValues.put(DBHelper.COLUMN_NAME_QUESTION, "Ce obligatii aveti la intalnirea indicatorului Animale?");
		contentValues.put(DBHelper.COLUMN_NAME_CATEGORY, DBHelper.CATEGORY_INDICATOARE_MARCAJE);
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_A, "A. Sa reduceti viteza numai la semnalul celor care insotesc animalele");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_B, "B. Circulati cu atentie, si daca se impune, reduceti viteza");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_C, "C. Nu aveti nicio obligatie, deoarece indicatorul se adreseaza persoanelor care insotesc animalele");
		contentValues.put(DBHelper.COLUMN_NAME_CORRECT_ANSWER, "B");

		db.insert(DBHelper.TABLE_NAME, null, contentValues);
		
		// q902 - C
		contentValues = new ContentValues();

		contentValues.put(DBHelper.COLUMN_NAME_QUESTION, "Ce semnificatie are indicatorul din imagine?");
		contentValues.put(DBHelper.COLUMN_NAME_CATEGORY, DBHelper.CATEGORY_INDICATOARE_MARCAJE);
		contentValues.put(DBHelper.COLUMN_NAME_IMAGE, R.drawable.q12c0);
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_A, "A. Trecere cu bacul peste o apa");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_B, "B. Drum denivelat");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_C, "C. Accesul interzis vehiculelor ce transporta substante de natura sa polueze apele");
		contentValues.put(DBHelper.COLUMN_NAME_CORRECT_ANSWER, "C");

		db.insert(DBHelper.TABLE_NAME, null, contentValues);

		// q922 - B
		contentValues = new ContentValues();

		contentValues.put(DBHelper.COLUMN_NAME_QUESTION, "Semnificatia unui indicator rutier, instalat deasupra unei benzi de circulatie, este valabila:");
		contentValues.put(DBHelper.COLUMN_NAME_CATEGORY, DBHelper.CATEGORY_INDICATOARE_MARCAJE);
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_A, "A. Numai pentru circulatia rutiera ce se desfasoara in tunele");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_B, "B. Numai pentru banda deasupra careia este instalat");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_C, "C. Pentru toata latimea partii carosabile");
		contentValues.put(DBHelper.COLUMN_NAME_CORRECT_ANSWER, "B");

		db.insert(DBHelper.TABLE_NAME, null, contentValues);

		// q894 - A
		contentValues = new ContentValues();

		contentValues.put(DBHelper.COLUMN_NAME_QUESTION, "Indicatorul din imagine semnifica:");
		contentValues.put(DBHelper.COLUMN_NAME_CATEGORY, DBHelper.CATEGORY_INDICATOARE_MARCAJE);
		contentValues.put(DBHelper.COLUMN_NAME_IMAGE, R.drawable.q13c0);
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_A, "A. Circulatie in ambele sensuri");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_B, "B. Circulatia interzisa in ambele sensuri");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_C, "C. Prioritate pentru circulatia din sens invers");
		contentValues.put(DBHelper.COLUMN_NAME_CORRECT_ANSWER, "A");

		db.insert(DBHelper.TABLE_NAME, null, contentValues);

		// q904 - C
		contentValues = new ContentValues();

		contentValues.put(DBHelper.COLUMN_NAME_QUESTION, "Marcajul longitudinal continuu, aplicat pe axul drumului, permite incalcarea lui:");
		contentValues.put(DBHelper.COLUMN_NAME_CATEGORY, DBHelper.CATEGORY_INDICATOARE_MARCAJE);
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_A, "A. Numai de catre conducatorii de motociclete si mopede");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_B, "B. Numai de catre vehiculele cu gabarit depasit");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_C, "C. nu, in nicio situatie");
		contentValues.put(DBHelper.COLUMN_NAME_CORRECT_ANSWER, "C");

		db.insert(DBHelper.TABLE_NAME, null, contentValues);

		// q896 - B
		contentValues = new ContentValues();

		contentValues.put(DBHelper.COLUMN_NAME_QUESTION, "Care dintre indicatoare obliga la schimbarea directiei de mers pe prima strada la dreapta:");
		contentValues.put(DBHelper.COLUMN_NAME_CATEGORY, DBHelper.CATEGORY_INDICATOARE_MARCAJE);
		contentValues.put(DBHelper.COLUMN_NAME_IMAGE, R.drawable.q14c0);
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_A, "A. Indicatorul 1");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_B, "B. Indicatorul 2");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_C, "C. Ambele indicatoare");
		contentValues.put(DBHelper.COLUMN_NAME_CORRECT_ANSWER, "B");

		db.insert(DBHelper.TABLE_NAME, null, contentValues);

		// q919 - A
		contentValues = new ContentValues();

		contentValues.put(DBHelper.COLUMN_NAME_QUESTION, "Unde veti parca autovehiculul la intalnirea acestui indicator?");
		contentValues.put(DBHelper.COLUMN_NAME_CATEGORY, DBHelper.CATEGORY_INDICATOARE_MARCAJE);
		contentValues.put(DBHelper.COLUMN_NAME_IMAGE, R.drawable.q15c0);
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_A, "A. Pe directia indicata, in parcarea special amenajata");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_B, "B. Pe trotuar, perpendicular pe acesta");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_C, "C. Pe trotuar, in lungul acestuia");
		contentValues.put(DBHelper.COLUMN_NAME_CORRECT_ANSWER, "A");

		db.insert(DBHelper.TABLE_NAME, null, contentValues);
		
		// q899 - A
		contentValues = new ContentValues();

		contentValues.put(DBHelper.COLUMN_NAME_QUESTION, "In aceasta situatie opriti:");
		contentValues.put(DBHelper.COLUMN_NAME_CATEGORY, DBHelper.CATEGORY_INDICATOARE_MARCAJE);
		contentValues.put(DBHelper.COLUMN_NAME_IMAGE, R.drawable.q16c0);
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_A, "A. La linia transversala");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_B, "B. In dreptul semaforului");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_C, "C. Pe trecerea pentru pietoni");
		contentValues.put(DBHelper.COLUMN_NAME_CORRECT_ANSWER, "A");

		db.insert(DBHelper.TABLE_NAME, null, contentValues);
		
		// q932 - B
		contentValues = new ContentValues();

		contentValues.put(DBHelper.COLUMN_NAME_QUESTION, "Ce obligatii aveti la intalnirea indicatorului Biciclisti?");
		contentValues.put(DBHelper.COLUMN_NAME_CATEGORY, DBHelper.CATEGORY_INDICATOARE_MARCAJE);
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_A, "A. Sa reduceti viteza la maximum 30 km/h");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_B, "B. Sa circulati cu atentie, si daca este cazul sa acordati prioritate biciclistilor, care circula pe pista special destinata");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_C, "C. Nu aveti nicio obligatie, biciclistii va vor acorda prioritate");
		contentValues.put(DBHelper.COLUMN_NAME_CORRECT_ANSWER, "B");

		db.insert(DBHelper.TABLE_NAME, null, contentValues);
		
		// q880 - C
		contentValues = new ContentValues();

		contentValues.put(DBHelper.COLUMN_NAME_QUESTION, "Care dintre indicatoare anunta sfarsitul drumului cu prioritate?");
		contentValues.put(DBHelper.COLUMN_NAME_CATEGORY, DBHelper.CATEGORY_INDICATOARE_MARCAJE);
		contentValues.put(DBHelper.COLUMN_NAME_IMAGE, R.drawable.q17c0);
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_A, "A. Niciunul");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_B, "B. Ambele");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_C, "C. Indicatorul 1");
		contentValues.put(DBHelper.COLUMN_NAME_CORRECT_ANSWER, "C");

		db.insert(DBHelper.TABLE_NAME, null, contentValues);
		
		// q936 - B
		contentValues = new ContentValues();

		contentValues.put(DBHelper.COLUMN_NAME_QUESTION, "Ce semnificatie are marcajul longitudinal aplicat sub forma unei linii continue pe axul drumurilor publice?");
		contentValues.put(DBHelper.COLUMN_NAME_CATEGORY, DBHelper.CATEGORY_INDICATOARE_MARCAJE);
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_A, "A. Ajuta conducatorii auto sa se orienteze pe timp de noapte");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_B, "B. Nu permite incalcarea lui de catre conducatorii auto");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_C, "C. Permite depasirea vehiculelor");
		contentValues.put(DBHelper.COLUMN_NAME_CORRECT_ANSWER, "B");

		db.insert(DBHelper.TABLE_NAME, null, contentValues);
  
		// q901 - A
		contentValues = new ContentValues();

		contentValues.put(DBHelper.COLUMN_NAME_QUESTION, "Indicatorul va avertizeaza ca:");
		contentValues.put(DBHelper.COLUMN_NAME_CATEGORY, DBHelper.CATEGORY_INDICATOARE_MARCAJE);
		contentValues.put(DBHelper.COLUMN_NAME_IMAGE, R.drawable.q18c0);
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_A, "A. Drumul se intersecteaza cu o trecere la nivel cu linii de tramvai");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_B, "B. Urmeaza o statie de tramvai cu refugiu pentru pietoni");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_C, "C. Urmeaza o trecere la nivel cu calea ferata fara bariere sau semibariere, cu semnalizare automata");
		contentValues.put(DBHelper.COLUMN_NAME_CORRECT_ANSWER, "A");

		db.insert(DBHelper.TABLE_NAME, null, contentValues);

		// q918 - B
		contentValues = new ContentValues();

		contentValues.put(DBHelper.COLUMN_NAME_QUESTION, "Ce semnifica indicatorul?");
		contentValues.put(DBHelper.COLUMN_NAME_CATEGORY, DBHelper.CATEGORY_INDICATOARE_MARCAJE);
		contentValues.put(DBHelper.COLUMN_NAME_IMAGE, R.drawable.q19c0);
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_A, "A. Drum aglomerat");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_B, "B. Interval obligatoriu intre autovehicule");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_C, "C. Distanta obligatorie de oprire");
		contentValues.put(DBHelper.COLUMN_NAME_CORRECT_ANSWER, "B");

		db.insert(DBHelper.TABLE_NAME, null, contentValues);
  
		// q911 - C
		contentValues = new ContentValues();

		contentValues.put(DBHelper.COLUMN_NAME_QUESTION, "Indicatorul pe care il intalniti va informeaza ca urmeaza:");
		contentValues.put(DBHelper.COLUMN_NAME_CATEGORY, DBHelper.CATEGORY_INDICATOARE_MARCAJE);
		contentValues.put(DBHelper.COLUMN_NAME_IMAGE, R.drawable.q20c0);
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_A, "A. O iesire de pe autostrada");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_B, "B. O statie de taxare pentru circulatia pe autostrazi");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_C, "C. Un loc special amenajat, unde puteti face o pauza de odihna");
		contentValues.put(DBHelper.COLUMN_NAME_CORRECT_ANSWER, "C");

		db.insert(DBHelper.TABLE_NAME, null, contentValues);
  
		// q897 - B
		contentValues = new ContentValues();

		contentValues.put(DBHelper.COLUMN_NAME_QUESTION, "In situatia data:");
		contentValues.put(DBHelper.COLUMN_NAME_CATEGORY, DBHelper.CATEGORY_INDICATOARE_MARCAJE);
		contentValues.put(DBHelper.COLUMN_NAME_IMAGE, R.drawable.q21c0);
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_A, "A. Veti circula cu prudenta pana la prima intersectie");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_B, "B. Veti circula cu maxim 30 km/h pe sectorul de drum denivelat");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_C, "C. Veti circula cu maxim 30 km/h");
		contentValues.put(DBHelper.COLUMN_NAME_CORRECT_ANSWER, "B");

		db.insert(DBHelper.TABLE_NAME, null, contentValues);
  
		//********************************************************* CAT 2 : Semnalele politistului
		
		//q1249 - B
		contentValues = new ContentValues();

		contentValues.put(DBHelper.COLUMN_NAME_QUESTION, "Ce indica semnalul agentului de circulatie?");
		contentValues.put(DBHelper.COLUMN_NAME_CATEGORY, DBHelper.CATEGORY_SEMNALELE_POLITISTULUI);
		contentValues.put(DBHelper.COLUMN_NAME_IMAGE, R.drawable.q1c1);
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_A, "A. Reduceti viteza");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_B, "B. Oprire");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_C, "C. Mariti viteza");
		contentValues.put(DBHelper.COLUMN_NAME_CORRECT_ANSWER, "B");

		db.insert(DBHelper.TABLE_NAME, null, contentValues);

		//q1139 - B
		contentValues = new ContentValues();

		contentValues.put(DBHelper.COLUMN_NAME_QUESTION, "Scoaterea, pe partea laterala din dreapta, a bastonului reflectorizant de catre politistul care insoteste o coloana oficiala de autovehicule obliga conducatorii care circula in spatele acesteia:");
		contentValues.put(DBHelper.COLUMN_NAME_CATEGORY, DBHelper.CATEGORY_SEMNALELE_POLITISTULUI);
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_A, "A. Sa reduca imediat viteza si sa circule cat mai aproape de marginea drumului");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_B, "B. Sa opreasca cat mai aproape de marginea drumului, iar acolo unde este posibil, sa scoata vehiculul in afara partii carosabile");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_C, "C. Sa reduca viteza si sa iasa in afara drumului imediat ce este posibil, prin viraj la dreapta");
		contentValues.put(DBHelper.COLUMN_NAME_CORRECT_ANSWER, "B");

		db.insert(DBHelper.TABLE_NAME, null, contentValues);

		//q995 - A
		contentValues = new ContentValues();

		contentValues.put(DBHelper.COLUMN_NAME_QUESTION, "Bratele intinse orizontal ale politistului care dirijeaza circulatia semnifica:");
		contentValues.put(DBHelper.COLUMN_NAME_CATEGORY, DBHelper.CATEGORY_SEMNALELE_POLITISTULUI);
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_A, "A. Oprire pentru toti participantii la trafic care, indiferent de sensul lor de mers, circula din directia sau directiile intersectate de bratele intinse ale acestuia");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_B, "B. Oprire pentru toti participantii la trafic care circula numai din spatele acestuia");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_C, "C. Oprire pentru toti participantii la trafic care circula numai din fata acestuia");
		contentValues.put(DBHelper.COLUMN_NAME_CORRECT_ANSWER, "A");

		db.insert(DBHelper.TABLE_NAME, null, contentValues);
		
		//q1353 - B
		contentValues = new ContentValues();

		contentValues.put(DBHelper.COLUMN_NAME_QUESTION, "Cum trebuie sa procedati in aceasta situatie?");
		contentValues.put(DBHelper.COLUMN_NAME_CATEGORY, DBHelper.CATEGORY_SEMNALELE_POLITISTULUI);
		contentValues.put(DBHelper.COLUMN_NAME_IMAGE, R.drawable.q2c1);
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_A, "A. Reduceti viteza si va continuati deplasarea");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_B, "B. Opriti si asteptati urmatorul semnal al politistului");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_C, "C. Circulati obligatoriu pe directia indicata de bratul politistului");
		contentValues.put(DBHelper.COLUMN_NAME_CORRECT_ANSWER, "B");

		db.insert(DBHelper.TABLE_NAME, null, contentValues);
		
		//q993 - C
		contentValues = new ContentValues();

		contentValues.put(DBHelper.COLUMN_NAME_QUESTION, "Ce semnificatie are, dupa coborarea bratelor, pozitia corpului unui politist care dirijeaza circulatia, pentru participantii la trafic care circula din fata si spatele sau?");
		contentValues.put(DBHelper.COLUMN_NAME_CATEGORY, DBHelper.CATEGORY_SEMNALELE_POLITISTULUI);
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_A, "A. Nicio semnificatie");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_B, "B. Permisiunea de a circula");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_C, "C. Oprire");
		contentValues.put(DBHelper.COLUMN_NAME_CORRECT_ANSWER, "C");

		db.insert(DBHelper.TABLE_NAME, null, contentValues);		

		//q1059 - B
		contentValues = new ContentValues();

		contentValues.put(DBHelper.COLUMN_NAME_QUESTION, "In situatia in care politistul se afla cu fata la un autovehicul care se apropie, avand bratul drept intins orizontal, conducatorul auto trebuie:");
		contentValues.put(DBHelper.COLUMN_NAME_CATEGORY, DBHelper.CATEGORY_SEMNALELE_POLITISTULUI);
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_A, "A. Sa opreasca, deoarece semnalul i se adreseaza");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_B, "B. Sa continue deplasarea, deoarece semnalul nu i se adreseaza");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_C, "C. Sa schimbe directia de mers spre stanga");
		contentValues.put(DBHelper.COLUMN_NAME_CORRECT_ANSWER, "B");

		db.insert(DBHelper.TABLE_NAME, null, contentValues);
		
		//q1336 - A
		contentValues = new ContentValues();

		contentValues.put(DBHelper.COLUMN_NAME_QUESTION, "Semnalul politistului rutier aflat intr-un autovehicul al politiei, efectuat cu bratul, cu sau fara baston reflectorizant, scos pe partea laterala dreapa a vehiculului, semnifica:");
		contentValues.put(DBHelper.COLUMN_NAME_CATEGORY, DBHelper.CATEGORY_SEMNALELE_POLITISTULUI);
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_A, "A. Oprire pentru conducatorii vehiculelor care circula in spatele autovehiculului politiei");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_B, "B. Reducerea vitezei de catre cei care circula in spatele autovehiculului politiei");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_C, "C. Semnalul se adreseaza celor care circula din sens opus");
		contentValues.put(DBHelper.COLUMN_NAME_CORRECT_ANSWER, "A");

		db.insert(DBHelper.TABLE_NAME, null, contentValues);

		//q1093 - A
		contentValues = new ContentValues();

		contentValues.put(DBHelper.COLUMN_NAME_QUESTION, "Daca politistul se afla in intersectie cu fata catre conducatorul autovehiculului, si ii adreseaza acestuia un alt semnal decat pozitia sa, care inseamna oprire, conducatorul trebuie:");
		contentValues.put(DBHelper.COLUMN_NAME_CATEGORY, DBHelper.CATEGORY_SEMNALELE_POLITISTULUI);
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_A, "A. Sa respecte semnalul politistului");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_B, "B. Sa continue mersul, deoarece semnele politistului sunt contradictorii");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_C, "C. Sa opreasca si sa astepte semnalul pentru continuarea depasirii");
		contentValues.put(DBHelper.COLUMN_NAME_CORRECT_ANSWER, "A");

		db.insert(DBHelper.TABLE_NAME, null, contentValues);

		//q1335 - C
		contentValues = new ContentValues();

		contentValues.put(DBHelper.COLUMN_NAME_QUESTION, "Care dintre tipurile de semnalizare de mai jos vor fi respectate cu prioritate?");
		contentValues.put(DBHelper.COLUMN_NAME_CATEGORY, DBHelper.CATEGORY_SEMNALELE_POLITISTULUI);
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_A, "A. Semnalele politistului");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_B, "B. Semnalizarea temporara, care modifica regimul normal de desfasurare a circulatiei");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_C, "C. Semnalele si indicatiile politistului");
		contentValues.put(DBHelper.COLUMN_NAME_CORRECT_ANSWER, "C");

		db.insert(DBHelper.TABLE_NAME, null, contentValues);

		//q1038 - A
		contentValues = new ContentValues();

		contentValues.put(DBHelper.COLUMN_NAME_QUESTION, "Cum procedati la semnalul politistului cu bratul ridicat vertical?");
		contentValues.put(DBHelper.COLUMN_NAME_CATEGORY, DBHelper.CATEGORY_SEMNALELE_POLITISTULUI);
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_A, "A. Opriti, indiferent din ce directie va apropiati, si asteptati urmatorul sau semnal");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_B, "B. Reduceti viteza si circulati in directia permisa de pozitia bratului");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_C, "C. Reduceti viteza si il ocoliti");
		contentValues.put(DBHelper.COLUMN_NAME_CORRECT_ANSWER, "A");

		db.insert(DBHelper.TABLE_NAME, null, contentValues);
		
		//q1216 - B
		contentValues = new ContentValues();

		contentValues.put(DBHelper.COLUMN_NAME_QUESTION, "Ce autovehicule pot patrunde in intersectie?");
		contentValues.put(DBHelper.COLUMN_NAME_CATEGORY, DBHelper.CATEGORY_SEMNALELE_POLITISTULUI);
		contentValues.put(DBHelper.COLUMN_NAME_IMAGE, R.drawable.q3c1);
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_A, "A. Troleibuzul si motocicleta");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_B, "B. Motocicleta si autocamionul");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_C, "C. Troleibuzul");
		contentValues.put(DBHelper.COLUMN_NAME_CORRECT_ANSWER, "B");

		db.insert(DBHelper.TABLE_NAME, null, contentValues);

		//q898 - B
		contentValues = new ContentValues();

		contentValues.put(DBHelper.COLUMN_NAME_QUESTION, "Semnalul politistului indica:");
		contentValues.put(DBHelper.COLUMN_NAME_CATEGORY, DBHelper.CATEGORY_SEMNALELE_POLITISTULUI);
		contentValues.put(DBHelper.COLUMN_NAME_IMAGE, R.drawable.q4c1);
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_A, "A. Circulatia pe directia inainte nu este posibila");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_B, "B. Oprire");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_C, "C. Circulatia prin intersectie se va face cu viteza redusa");
		contentValues.put(DBHelper.COLUMN_NAME_CORRECT_ANSWER, "B");

		db.insert(DBHelper.TABLE_NAME, null, contentValues);
		
		//q1276 - A
		contentValues = new ContentValues();

		contentValues.put(DBHelper.COLUMN_NAME_QUESTION, "Ce trebuie sa faca conducatorul auto in situatia prezentata:");
		contentValues.put(DBHelper.COLUMN_NAME_CATEGORY, DBHelper.CATEGORY_SEMNALELE_POLITISTULUI);
		contentValues.put(DBHelper.COLUMN_NAME_IMAGE, R.drawable.q5c1);
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_A, "A. Sa reduca viteza");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_B, "B. Sa opreasca");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_C, "C. Sa nu depaseasca motocicleta de politie");
		contentValues.put(DBHelper.COLUMN_NAME_CORRECT_ANSWER, "A");

		db.insert(DBHelper.TABLE_NAME, null, contentValues);		
		
		//q1235 - C
		contentValues = new ContentValues();

		contentValues.put(DBHelper.COLUMN_NAME_QUESTION, "Care dintre autovehicule au dreptul sa intre in intersectie?");
		contentValues.put(DBHelper.COLUMN_NAME_CATEGORY, DBHelper.CATEGORY_SEMNALELE_POLITISTULUI);
		contentValues.put(DBHelper.COLUMN_NAME_IMAGE, R.drawable.q6c1);
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_A, "A. Autocamionul si motocicleta");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_B, "B. Autoturismul si autocamionul");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_C, "C. Autoturismul si motocicleta");
		contentValues.put(DBHelper.COLUMN_NAME_CORRECT_ANSWER, "C");

		db.insert(DBHelper.TABLE_NAME, null, contentValues);
		
		//q1166 - B
		contentValues = new ContentValues();

		contentValues.put(DBHelper.COLUMN_NAME_QUESTION, "Ce comportament este corect in situatia prezentata, daca va aflati la volanul autoturismului verde?");
		contentValues.put(DBHelper.COLUMN_NAME_CATEGORY, DBHelper.CATEGORY_SEMNALELE_POLITISTULUI);
		contentValues.put(DBHelper.COLUMN_NAME_IMAGE, R.drawable.q7c1);
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_A, "A. Intrati cu atentie in intersectie si virati numai la stanga");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_B, "B. Opriti si nu intrati in intersectie");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_C, "C. Aveti voie sa virati la dreapta");
		contentValues.put(DBHelper.COLUMN_NAME_CORRECT_ANSWER, "B");

		db.insert(DBHelper.TABLE_NAME, null, contentValues);

		//q1477 - A
		contentValues = new ContentValues();

		contentValues.put(DBHelper.COLUMN_NAME_QUESTION, "Cand politistul se afla cu spatele la autovehiculul care se apropie si cu bratul stang intins orizontal, conducatorul acestuia trebuie:");
		contentValues.put(DBHelper.COLUMN_NAME_CATEGORY, DBHelper.CATEGORY_SEMNALELE_POLITISTULUI);
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_A, "A. Sa continue deplasarea, deoarece semnalul de oprire nu i se adreseaza");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_B, "B. Sa schimbe directia de mers spre stanga");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_C, "C. Sa opreasca, deoarece semnalul i se adreseaza");
		contentValues.put(DBHelper.COLUMN_NAME_CORRECT_ANSWER, "A");

		db.insert(DBHelper.TABLE_NAME, null, contentValues);
		
		//q1404 - C
		contentValues = new ContentValues();

		contentValues.put(DBHelper.COLUMN_NAME_QUESTION, "Cum procedati in situatia in care semnalul rosu al semaforului este in functiune, iar politistul va adreseaza semnalul de a trece ?");
		contentValues.put(DBHelper.COLUMN_NAME_CATEGORY, DBHelper.CATEGORY_SEMNALELE_POLITISTULUI);
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_A, "A. Asteptati aparitia semnalului de culoare verde, care va permite sa circulati");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_B, "B. Atrageti atentia politistului ca exista neconcordanta intre semnalul sau si cel al semaforului");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_C, "C. Respectati semnalul politistului");
		contentValues.put(DBHelper.COLUMN_NAME_CORRECT_ANSWER, "C");

		db.insert(DBHelper.TABLE_NAME, null, contentValues);		

		//q1567 - A
		contentValues = new ContentValues();

		contentValues.put(DBHelper.COLUMN_NAME_QUESTION, "Bratul stang, intins orizontal, al politistului semnifica oprire pentru: ");
		contentValues.put(DBHelper.COLUMN_NAME_CATEGORY, DBHelper.CATEGORY_SEMNALELE_POLITISTULUI);
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_A, "A. Vehiculele care vin din fata");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_B, "B. Vehiculele care vin din spate");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_C, "C. Vehiculele care vin din fata si din spate");
		contentValues.put(DBHelper.COLUMN_NAME_CORRECT_ANSWER, "A");

		db.insert(DBHelper.TABLE_NAME, null, contentValues);

		//q1673 - B
		contentValues = new ContentValues();

		contentValues.put(DBHelper.COLUMN_NAME_QUESTION, "La semnalul politistului avand bratul stang intins orizontal, orientat cu fata catre autovehiculul care se apropie, conducatorul auto trebuie: ");
		contentValues.put(DBHelper.COLUMN_NAME_CATEGORY, DBHelper.CATEGORY_SEMNALELE_POLITISTULUI);
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_A, "A. Sa reduca viteza");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_B, "B. Sa opreasca imediat");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_C, "C. Sa continue deplasarea");
		contentValues.put(DBHelper.COLUMN_NAME_CORRECT_ANSWER, "B");

		db.insert(DBHelper.TABLE_NAME, null, contentValues);
		
		//q1487 - A
		contentValues = new ContentValues();

		contentValues.put(DBHelper.COLUMN_NAME_QUESTION, "Politistul nu permite trecerea: ");
		contentValues.put(DBHelper.COLUMN_NAME_CATEGORY, DBHelper.CATEGORY_SEMNALELE_POLITISTULUI);
		contentValues.put(DBHelper.COLUMN_NAME_IMAGE, R.drawable.q8c1);
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_A, "A. Tramvaiului");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_B, "B. Camionului");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_C, "C. Autoturismului verde");
		contentValues.put(DBHelper.COLUMN_NAME_CORRECT_ANSWER, "A");

		db.insert(DBHelper.TABLE_NAME, null, contentValues);
		
		//q1628 - B
		contentValues = new ContentValues();

		contentValues.put(DBHelper.COLUMN_NAME_QUESTION, "Cum procedati daca, intr-o intersectie, politistul este orientat cu spatele catre dvs si cu bratul stang intins orizontal ?");
		contentValues.put(DBHelper.COLUMN_NAME_CATEGORY, DBHelper.CATEGORY_SEMNALELE_POLITISTULUI);
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_A, "A. Virati la stanga");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_B, "B. Continuati drumul");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_C, "C. Reduceti viteza autovehiculului si opriti in spatele acestuia");
		contentValues.put(DBHelper.COLUMN_NAME_CORRECT_ANSWER, "B");

		db.insert(DBHelper.TABLE_NAME, null, contentValues);
		
		//q1518 - B
		contentValues = new ContentValues();

		contentValues.put(DBHelper.COLUMN_NAME_QUESTION, "Care dintre vehicule trebuie sa opreasca la semnalul politistului ?");
		contentValues.put(DBHelper.COLUMN_NAME_CATEGORY, DBHelper.CATEGORY_SEMNALELE_POLITISTULUI);
		contentValues.put(DBHelper.COLUMN_NAME_IMAGE, R.drawable.q9c1);
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_A, "A. Toate vehiculele");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_B, "B. Tramvaiele si autoturismul");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_C, "C. Bicicleta");
		contentValues.put(DBHelper.COLUMN_NAME_CORRECT_ANSWER, "B");

		db.insert(DBHelper.TABLE_NAME, null, contentValues);
		
		//q1550 - A
		contentValues = new ContentValues();

		contentValues.put(DBHelper.COLUMN_NAME_QUESTION, "Politistul din intersectie semnalizeaza conducatorul auto sa mareasca viteza. Cum trebuie sa procedeze acesta ?");
		contentValues.put(DBHelper.COLUMN_NAME_CATEGORY, DBHelper.CATEGORY_SEMNALELE_POLITISTULUI);
		contentValues.put(DBHelper.COLUMN_NAME_IMAGE, R.drawable.q10c1);
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_A, "A. Sa respecte semnalul politistului");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_B, "B. Sa astepte lumina verde a semaforului");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_C, "C. Sa acorde prioritate, apoi sa continue deplasarea");
		contentValues.put(DBHelper.COLUMN_NAME_CORRECT_ANSWER, "A");

		db.insert(DBHelper.TABLE_NAME, null, contentValues);

		//q1670 - A
		contentValues = new ContentValues();

		contentValues.put(DBHelper.COLUMN_NAME_QUESTION, "Rotirea vioaie a bratului politistului semnifica:");
		contentValues.put(DBHelper.COLUMN_NAME_CATEGORY, DBHelper.CATEGORY_SEMNALELE_POLITISTULUI);
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_A, "A. Marirea vitezei de deplasare sau grabirea traversarii drumului de catre pietoni");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_B, "B. Oprirea temporara a circulatiei");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_C, "C. Reducerea vitezei de deplasare");
		contentValues.put(DBHelper.COLUMN_NAME_CORRECT_ANSWER, "A");

		db.insert(DBHelper.TABLE_NAME, null, contentValues);
		
		//q1651 - C
		contentValues = new ContentValues();

		contentValues.put(DBHelper.COLUMN_NAME_QUESTION, "Autoturismul poate patrunde in intersectia prezentata?");
		contentValues.put(DBHelper.COLUMN_NAME_CATEGORY, DBHelper.CATEGORY_SEMNALELE_POLITISTULUI);
		contentValues.put(DBHelper.COLUMN_NAME_IMAGE, R.drawable.q11c1);
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_A, "A. Da");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_B, "B. Numai in caz de urgenta");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_C, "C. Nu, in intersectie poate patrunde numai bicicleta");
		contentValues.put(DBHelper.COLUMN_NAME_CORRECT_ANSWER, "C");

		db.insert(DBHelper.TABLE_NAME, null, contentValues);
		
		//q1489 - B
		contentValues = new ContentValues();

		contentValues.put(DBHelper.COLUMN_NAME_QUESTION, "Semnalul politistului indica: ");
		contentValues.put(DBHelper.COLUMN_NAME_CATEGORY, DBHelper.CATEGORY_SEMNALELE_POLITISTULUI);
		contentValues.put(DBHelper.COLUMN_NAME_IMAGE, R.drawable.q12c1);
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_A, "A. Reduceti viteza");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_B, "B. Mariti viteza");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_C, "C. Mergeti cu prudenta");
		contentValues.put(DBHelper.COLUMN_NAME_CORRECT_ANSWER, "B");

		db.insert(DBHelper.TABLE_NAME, null, contentValues);
		
		//q1631 - B
		contentValues = new ContentValues();

		contentValues.put(DBHelper.COLUMN_NAME_QUESTION, "Semnalul agentilor cailor ferate, executat cu un fanion rosu, aflati la trecerea la nivel cu o cale ferata industriala din orase, va obliga: ");
		contentValues.put(DBHelper.COLUMN_NAME_CATEGORY, DBHelper.CATEGORY_SEMNALELE_POLITISTULUI);
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_A, "A. Sa reduceti viteza, sa va asigurati, iar daca trenul este la mai mult de 50 m de pasaj, sa treceti cu atentie");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_B, "B. Sa opriti");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_C, "C. Nu aveti nicio obligatie, deoarece agentul dirijeaza manevrele garniturii de tren");
		contentValues.put(DBHelper.COLUMN_NAME_CORRECT_ANSWER, "B");

		db.insert(DBHelper.TABLE_NAME, null, contentValues);		
		
		//q1700 - C
		contentValues = new ContentValues();

		contentValues.put(DBHelper.COLUMN_NAME_QUESTION, "Scoaterea pe partea laterala a bastonului de catre politistul care insoteste o coloana oficiala obliga conducatorul: ");
		contentValues.put(DBHelper.COLUMN_NAME_CATEGORY, DBHelper.CATEGORY_SEMNALELE_POLITISTULUI);
		contentValues.put(DBHelper.COLUMN_NAME_IMAGE, R.drawable.q13c1);
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_A, "A. Sa reduca viteza si sa opreasca in afara drumului imediat ce este posibil");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_B, "B. Sa reduca viteza si sa circule cat mai aproape de marginea drumului");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_C, "C. Sa opreasca autovehiculul cat mai aproape de marginea drumului");
		contentValues.put(DBHelper.COLUMN_NAME_CORRECT_ANSWER, "C");

		db.insert(DBHelper.TABLE_NAME, null, contentValues);

		//q1551 - C
		contentValues = new ContentValues();

		contentValues.put(DBHelper.COLUMN_NAME_QUESTION, "Ce indica aceasta pozitie a politistului rutier: ");
		contentValues.put(DBHelper.COLUMN_NAME_CATEGORY, DBHelper.CATEGORY_SEMNALELE_POLITISTULUI);
		contentValues.put(DBHelper.COLUMN_NAME_IMAGE, R.drawable.q14c1);
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_A, "A. Reduceti viteza");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_B, "B. Mariti viteza");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_C, "C. Oprire");
		contentValues.put(DBHelper.COLUMN_NAME_CORRECT_ANSWER, "C");

		db.insert(DBHelper.TABLE_NAME, null, contentValues);

		//q1561 - B
		contentValues = new ContentValues();

		contentValues.put(DBHelper.COLUMN_NAME_QUESTION, "Ce semnifica semnalul politistului aflat intr-o intersectie, cu bratul drept intins orizontal: ");
		contentValues.put(DBHelper.COLUMN_NAME_CATEGORY, DBHelper.CATEGORY_SEMNALELE_POLITISTULUI);
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_A, "A. Oprire pentru vehiculele si pietonii care vin din fata acestuia");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_B, "B. Oprire pentru vehiculele si pietonii care vin din spatele acestuia");
		contentValues.put(DBHelper.COLUMN_NAME_ANSWER_C, "C. Oprire pentru vehiculele si pietonii care vin din fata si spatele acestuia");
		contentValues.put(DBHelper.COLUMN_NAME_CORRECT_ANSWER, "B");

		db.insert(DBHelper.TABLE_NAME, null, contentValues);
		
  }
  
}
