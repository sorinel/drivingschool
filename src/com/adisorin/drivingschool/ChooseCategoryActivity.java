package com.adisorin.drivingschool;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class ChooseCategoryActivity extends ListActivity {
	
	private static final String[] CATEGORIES = {"Indicatoare si marcaje", "Semnalele politistului", "Semnale luminoase", "Depasirea", 
		"Manevre in timpul mersului", "Oprire, stationare si parcare", "Trecerea la nivel cu calea ferata", "Circulatia pe autostrazi", 
		"Mecanica", "Sanctiuni si infractiuni", "Conducere preventiva"}; 
	private static final String TAG = ChooseCategoryActivity.class.getName();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.activity_choose_category);
	    ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, CATEGORIES);
	    setListAdapter(adapter);
	}
	
	@Override
	protected void onStart() {
	    super.onStart();
	}
	
	@Override
	protected void onListItemClick(ListView listView, View view, int position, long id) {
	    super.onListItemClick(listView, view, position, id);
	    
	    //start DrivingTestActivity with a defined category
	    Intent startDrivingTestIntent = new Intent(ChooseCategoryActivity.this, DrivingTestActivity.class);
	    switch(position) {
	    case DBHelper.CATEGORY_INDICATOARE_MARCAJE:
	    	startDrivingTestIntent.putExtra(DBHelper.CATEGORY, DBHelper.CATEGORY_INDICATOARE_MARCAJE);
	    	break;
	    case DBHelper.CATEGORY_SEMNALELE_POLITISTULUI:
	    	startDrivingTestIntent.putExtra(DBHelper.CATEGORY, DBHelper.CATEGORY_SEMNALELE_POLITISTULUI);
	    	break;
	    case DBHelper.CATEGORY_SEMNALE_LUMINOASE:
	    	startDrivingTestIntent.putExtra(DBHelper.CATEGORY, DBHelper.CATEGORY_SEMNALE_LUMINOASE);
	    	break;
	    case DBHelper.CATEGORY_DEPASIREA:
	    	startDrivingTestIntent.putExtra(DBHelper.CATEGORY, DBHelper.CATEGORY_DEPASIREA);
	    	break;
	    case DBHelper.CATEGORY_MANEVRE_IN_TIMPUL_MERSULUI:
	    	startDrivingTestIntent.putExtra(DBHelper.CATEGORY, DBHelper.CATEGORY_MANEVRE_IN_TIMPUL_MERSULUI);
	    	break;	
	    case DBHelper.CATEGORY_OPRIRE_STATIONARE_PARCARE:
	    	startDrivingTestIntent.putExtra(DBHelper.CATEGORY, DBHelper.CATEGORY_OPRIRE_STATIONARE_PARCARE);
	    	break;
	    case DBHelper.CATEGORY_TRECEREA_LA_NIVEL_CU_CALEA_FERATA:
	    	startDrivingTestIntent.putExtra(DBHelper.CATEGORY, DBHelper.CATEGORY_TRECEREA_LA_NIVEL_CU_CALEA_FERATA);
	    	break;
	    case DBHelper.CATEGORY_CIRCULATIA_PE_AUTOSTRAZI:
	    	startDrivingTestIntent.putExtra(DBHelper.CATEGORY, DBHelper.CATEGORY_CIRCULATIA_PE_AUTOSTRAZI);
	    	break;
	    case DBHelper.CATEGORY_MECANICA:
	    	startDrivingTestIntent.putExtra(DBHelper.CATEGORY, DBHelper.CATEGORY_MECANICA);
	    	break;
	    case DBHelper.CATEGORY_SANCTIUNI_INFRACTIUNI:
	    	startDrivingTestIntent.putExtra(DBHelper.CATEGORY, DBHelper.CATEGORY_SANCTIUNI_INFRACTIUNI);
	    	break;
	    case DBHelper.CATEGORY_CONDUCERE_PREVENTIVA:
	    	startDrivingTestIntent.putExtra(DBHelper.CATEGORY, DBHelper.CATEGORY_CONDUCERE_PREVENTIVA);
	    	break;
	    }
	    
	    //first time starting test
	    DrivingTestActivity.mIsFirstEntry = true;
	    startActivity(startDrivingTestIntent);
	}
	
	@Override
	protected void onDestroy() {
	    super.onDestroy();
	}
	
}
