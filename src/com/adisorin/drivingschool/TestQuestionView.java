package com.adisorin.drivingschool;

public class TestQuestionView {

  private int id;
  private int photoResourceId;
  private String question;
  private String answerA;
  private String answerB;
  private String answerC;
  private String correctAnswer;
  
  public TestQuestionView() {
  
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public int getPhotoResourceId() {
    return photoResourceId;
  }

  public void setPhotoResourceId(int photoResourceId) {
    this.photoResourceId = photoResourceId;
  }

  public String getQuestion() {
    return question;
  }

  public void setQuestion(String question) {
    this.question = question;
  }

  public String getAnswerA() {
    return answerA;
  }

  public void setAnswerA(String answerA) {
    this.answerA = answerA;
  }

  public String getAnswerB() {
    return answerB;
  }

  public void setAnswerB(String answerB) {
    this.answerB = answerB;
  }

  public String getAnswerC() {
    return answerC;
  }

  public void setAnswerC(String answerC) {
    this.answerC = answerC;
  }

public String getCorrectAnswer() {
	return correctAnswer;
}

public void setCorrectAnswer(String correctAnswer) {
	this.correctAnswer = correctAnswer;
}

}
