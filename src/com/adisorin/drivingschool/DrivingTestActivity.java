package com.adisorin.drivingschool;

import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class DrivingTestActivity extends Activity {

    private static final int EVENT_TIMEOUT = 1;

	private ImageView mTestImage;
	private TextView mTestQuestion;
	private TextView mTestAnswerA;
	private TextView mTestAnswerB;
	private TextView mTestAnswerC;
	private Button mButtonAnswerA;
	private Button mButtonAnswerB;
	private Button mButtonAnswerC;
	private Button mButtonFinishTest;
	private boolean mIsButtonASelected = false;
	private boolean mIsButtonBSelected = false;
	private boolean mIsButtonCSelected = false;
	private Message mMessage = null;
	static boolean mIsFirstEntry;
	private int category;
	private static final int DEFAULT_CATEGORY= -1;
	private static ArrayList<Integer> sQuestionIDsList;
	private ArrayList<WrongQuestion> mWrongQuestions;
	private static int sCurrentQestionIdIndex = -1;
	private final String TAG = "DrivingTestActivity";
	private CountDownTimer mTimer;
	private TestQuestionView mCurrentQuestion;

	private static final int NUMBER_OF_CATEGORIES = 11;
	private static final int NUMBER_OF_QUESTIONS = 26;
	private static final int NUMBER_OF_MINIMUM_CORRECT_QUESTIONS = 22;
	private static final int NUMBER_OF_MAXIMUM_QUESTIONS_PER_CATEGORY = 5;

	private OnClickListener mAnswerButtonListener = new OnClickListener() {	
		@Override
		public void onClick(View view) {
			if(view == mButtonAnswerA && !mIsButtonASelected) {
				mButtonAnswerA.setBackgroundColor(Color.CYAN);
				mIsButtonASelected = true;
			} else if(view == mButtonAnswerA && mIsButtonASelected) {
				mButtonAnswerA.setBackgroundColor(Color.BLUE);
				mIsButtonASelected = false;
			}

			if(view == mButtonAnswerB && !mIsButtonBSelected) {
				mButtonAnswerB.setBackgroundColor(Color.CYAN);
				mIsButtonBSelected = true;
			} else if(view == mButtonAnswerB && mIsButtonBSelected) {
				mButtonAnswerB.setBackgroundColor(Color.BLUE);
				mIsButtonBSelected = false;
			}

			if(view == mButtonAnswerC && !mIsButtonCSelected) {
				mButtonAnswerC.setBackgroundColor(Color.CYAN);
				mIsButtonCSelected = true;
			} else if(view == mButtonAnswerC && mIsButtonCSelected) {
				mButtonAnswerC.setBackgroundColor(Color.BLUE);
				mIsButtonCSelected = false;
			}

		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_driving_test);
		setupUI();
		mWrongQuestions = new ArrayList<DrivingTestActivity.WrongQuestion>();

	    mTimer = new CountDownTimer(60000*30,1000) {

	        TextView time = (TextView) findViewById(R.id.timer);

	         public void onTick(long millisUntilFinished) {
	             time.setText(""+String.format("%d min : %d sec",
	                     TimeUnit.MILLISECONDS.toMinutes( millisUntilFinished),
	                     TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
	                     TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));;
	         }

	         public void onFinish() {
	             mMessage = Message.obtain(handler, EVENT_TIMEOUT);
	             handler.sendMessage(mMessage);
	         }
	  }.start();
		//initialize list at the start of each set of tests, not during the test
		if(mIsFirstEntry) {
			sQuestionIDsList = new ArrayList<Integer>();
			// check if test is by category, and if it is, load questions
			checkTypeOfTest();
		}
	}

	@Override
	protected void onStart() {
		super.onStart();
		
		boolean isEndOfTest = sQuestionIDsList.size() == 0 && !mIsFirstEntry ;
		boolean isEmptyCategory = sQuestionIDsList.size() == 0 && mIsFirstEntry;
		if(isEndOfTest || isEmptyCategory) {
			if(mWrongQuestions.size() > 0) {
				showWrongQuestionsDialog();
			} else {
				Toast.makeText(this, "test complet!", Toast.LENGTH_LONG).show();
				finish();
			}
			return;
		}

		if(sQuestionIDsList.size() != 1) {
				// get random question
				sCurrentQestionIdIndex = new Random().nextInt(sQuestionIDsList.size() - 1) + 1;
		} else {
			sCurrentQestionIdIndex = 0;
		}
		
		mCurrentQuestion = getQuestionById(sQuestionIDsList.get(sCurrentQestionIdIndex));

		mTestImage.setImageResource(mCurrentQuestion.getPhotoResourceId());
		mTestQuestion.setText(mCurrentQuestion.getQuestion());
		mTestAnswerA.setText(mCurrentQuestion.getAnswerA());
		mTestAnswerB.setText(mCurrentQuestion.getAnswerB());
		mTestAnswerC.setText(mCurrentQuestion.getAnswerC());
		mButtonAnswerA.setOnClickListener(mAnswerButtonListener);
		mButtonAnswerB.setOnClickListener(mAnswerButtonListener);
		mButtonAnswerC.setOnClickListener(mAnswerButtonListener);

		// go to next test, validate results, and remove current question id from the questions list
		mButtonFinishTest.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (!mIsButtonASelected && !mIsButtonBSelected && !mIsButtonCSelected) {
					showNoAnswerSelectedDialog();
				} else {
					validateResults();
					if (sQuestionIDsList.size() > 0) {
						sQuestionIDsList.remove(Integer.valueOf(sQuestionIDsList.get(sCurrentQestionIdIndex)));
						onStart();
						clearAnswer();
						mIsFirstEntry = false;
					} else {
						showWrongQuestionsDialog();
					}

				}
			}
		});

	}

	private void validateResults() {
		String correctAnswer = mCurrentQuestion.getCorrectAnswer();
		String userAnswer = getUserAnswer();
		if(!correctAnswer.equals(userAnswer)) {
			WrongQuestion wrongQuestion = new WrongQuestion();
			wrongQuestion.setQuestionId(mCurrentQuestion.getId());
			wrongQuestion.setUserAnswer(userAnswer);
			mWrongQuestions.add(wrongQuestion);
		}
	}
	
	private String getUserAnswer() {
		String userAnswers = "";
		if(mIsButtonASelected) {
			userAnswers += DBHelper.ANSWER_A;
		}
		if(mIsButtonBSelected) {
			userAnswers += DBHelper.ANSWER_SEPARATOR + DBHelper.ANSWER_B;
		}
		if(mIsButtonCSelected) {
			userAnswers += DBHelper.ANSWER_SEPARATOR + DBHelper.ANSWER_C;
		}
		return userAnswers;
	}

	private void showWrongQuestionsDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.test_summary)
        	.setMessage("Ati gresit " + mWrongQuestions.size() + " intrebari. Rugati-l pe Adi sa le afiseze :)" )
               .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                   public void onClick(DialogInterface dialog, int id) {
                	   DrivingTestActivity.this.finish();
                   }
               }).setCancelable(false);
        builder.create();
        builder.show();
	}

	private void showNoAnswerSelectedDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.test_summary)
            .setMessage("Va rugam sa alegeti un raspuns" )
            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
               @Override
               public void onClick(DialogInterface dialog, int which) {

               }
             })
            .setCancelable(false);
        builder.create();
        builder.show();
	}

	public void clearAnswer() {
        mButtonAnswerA.setBackgroundColor(Color.BLUE);
        mButtonAnswerB.setBackgroundColor(Color.BLUE);
        mButtonAnswerC.setBackgroundColor(Color.BLUE);
        mIsButtonASelected = false;
        mIsButtonBSelected = false;
        mIsButtonCSelected = false;
	}

    private void showToast(String msg) {
        Toast toast;
        toast = Toast.makeText(this,"", Toast.LENGTH_LONG);
        toast.setText(msg);
        toast.show();
    }

    Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case EVENT_TIMEOUT:
                    showToast("Din pacate timpul a expirat!!");
                    break;
            }
        }
    };

	@Override
	protected void onDestroy() {
	    super.onDestroy();
	    mIsFirstEntry = false;
	}
	
	private void setupUI() {
		mTestImage = (ImageView) findViewById(R.id.image);

		mTestQuestion = (TextView) findViewById(R.id.question);
		mTestAnswerA = (TextView) findViewById(R.id.answer1);
		mTestAnswerB = (TextView) findViewById(R.id.answer2);
		mTestAnswerC = (TextView) findViewById(R.id.answer3);
		mButtonAnswerA = (Button) findViewById(R.id.buttonAnswerA);
		mButtonAnswerB = (Button) findViewById(R.id.buttonAnswerB);
		mButtonAnswerC = (Button) findViewById(R.id.buttonAnswerC);
		mButtonFinishTest = (Button) findViewById(R.id.buttonFinishTest);
	}
	
	private void checkTypeOfTest() {
		Intent receivedIntent = getIntent();
		// if a category was selected
		if(receivedIntent.getExtras() != null) {
			startTestByCategory(receivedIntent);
		} else {
			startNormalTest();
		}
	}

	private void startTestByCategory(Intent receivedIntent) {
	    category = receivedIntent.getExtras().getInt(DBHelper.CATEGORY, DEFAULT_CATEGORY);
		if(category != DEFAULT_CATEGORY) {
			loadQuestionsByCategory(category);
		}
    }

	private void startNormalTest() {
		Random random = new Random();
		int numberOfQuestions = 0;

		while(numberOfQuestions < NUMBER_OF_QUESTIONS) {
			int currentCategory;
			ArrayList<Integer> currentQuestionIDsList;
			int currrentNumberOfQuestions = random.nextInt(NUMBER_OF_MAXIMUM_QUESTIONS_PER_CATEGORY);

			// get a category with at least a question in it
			do {
				currentCategory = random.nextInt(NUMBER_OF_CATEGORIES);
				currentQuestionIDsList = getQuestionIDsByCategory(currentCategory);
			} while(currentQuestionIDsList.size() == 0);

			// check if we are asking for more questions than the category has
			if(currrentNumberOfQuestions > currentQuestionIDsList.size()) {
				currrentNumberOfQuestions = currentQuestionIDsList.size();
			}

			Log.d(TAG, "Got " + currrentNumberOfQuestions + " questions from category " + currentCategory);
			// get random category ids
			for(int i = 0; i < currrentNumberOfQuestions; i++) {
				// check for overflow
				if(numberOfQuestions == NUMBER_OF_QUESTIONS) {
					break;
				}

				numberOfQuestions++;
				int currentQuestionIdIndex = random.nextInt(currentQuestionIDsList.size());

				// check if we already have this question
				while(sQuestionIDsList.contains(currentQuestionIDsList.get(currentQuestionIdIndex))) {
					currentQuestionIdIndex = random.nextInt(currentQuestionIDsList.size());
				}
				sQuestionIDsList.add(currentQuestionIDsList.get(currentQuestionIdIndex));
			}
		}
	}

	private void loadQuestionsByCategory(int category) {
		final String[] projection = {DBHelper.COLUMN_NAME_ID};
		final String WHERE_CLAUSE = DBHelper.COLUMN_NAME_CATEGORY + "=" + category;
		
		Cursor cursor = HomeActivity.getDatabase().query(DBHelper.TABLE_NAME, 
				projection, WHERE_CLAUSE, null, null, null, null);
		cursor.moveToFirst();
		
		while(cursor.moveToNext()) {
			int column_id = cursor.getColumnIndex(DBHelper.COLUMN_NAME_ID);
			int id = cursor.getInt(column_id);
			sQuestionIDsList.add(id);
		}
	}

	private ArrayList<Integer> getQuestionIDsByCategory(int categoryId) {
		ArrayList<Integer> questionIDs = new ArrayList<Integer>();

		final String[] projection = {DBHelper.COLUMN_NAME_ID};
		final String WHERE_CLAUSE = DBHelper.COLUMN_NAME_CATEGORY + "=" + categoryId;

		Cursor cursor = HomeActivity.getDatabase().query(DBHelper.TABLE_NAME, 
				projection, WHERE_CLAUSE, null, null, null, null);
		cursor.moveToFirst();

		while(cursor.moveToNext()) {
			questionIDs.add(cursor.getInt(cursor.getColumnIndex(DBHelper.COLUMN_NAME_ID)));
		}

		return questionIDs;
	}

	private TestQuestionView getQuestionById(int id) {	
		TestQuestionView testQuestion = new TestQuestionView();

		final String projection[] = { DBHelper.COLUMN_NAME_ID,
				DBHelper.COLUMN_NAME_IMAGE, DBHelper.COLUMN_NAME_QUESTION,
				DBHelper.COLUMN_NAME_ANSWER_A, DBHelper.COLUMN_NAME_ANSWER_B,
				DBHelper.COLUMN_NAME_ANSWER_C , DBHelper.COLUMN_NAME_CORRECT_ANSWER};

		final String ID_WHERE_CLAUSE = DBHelper.COLUMN_NAME_ID + "=" + id; 

		Cursor cursor = HomeActivity.getDatabase().query(DBHelper.TABLE_NAME,
				projection, ID_WHERE_CLAUSE, null, null, null, null);

		if(cursor.getCount() > 0) {
			cursor.moveToFirst();

			int cursor_index_id = cursor.getColumnIndex(DBHelper.COLUMN_NAME_ID);
			int cursor_index_image = cursor.getColumnIndex(DBHelper.COLUMN_NAME_IMAGE);
			int cursor_index_question = cursor.getColumnIndex(DBHelper.COLUMN_NAME_QUESTION);
			int cursor_index_answerA = cursor.getColumnIndex(DBHelper.COLUMN_NAME_ANSWER_A);
			int cursor_index_answerB = cursor.getColumnIndex(DBHelper.COLUMN_NAME_ANSWER_B);
			int cursor_index_answerC = cursor.getColumnIndex(DBHelper.COLUMN_NAME_ANSWER_C);
			int cursor_index_correct_answer = cursor.getColumnIndex(DBHelper.COLUMN_NAME_CORRECT_ANSWER);

			int questionId = cursor.getInt(cursor_index_id);
			int image = cursor.getInt(cursor_index_image);
			String question = cursor.getString(cursor_index_question);
			String answerA = cursor.getString(cursor_index_answerA);
			String answerB = cursor.getString(cursor_index_answerB);
			String answerC = cursor.getString(cursor_index_answerC);
			String correctAnswer = cursor.getString(cursor_index_correct_answer);

			testQuestion.setId(questionId);
			testQuestion.setPhotoResourceId(image);
			testQuestion.setQuestion(question);
			testQuestion.setAnswerA(answerA);
			testQuestion.setAnswerB(answerB);
			testQuestion.setAnswerC(answerC);
			testQuestion.setCorrectAnswer(correctAnswer);
		}

		return testQuestion;
	}
	
	private class WrongQuestion {
		private int questionId;
		private String userAnswer;

		public int getQuestionId() {
			return questionId;
		}

		public void setQuestionId(int questionId) {
			this.questionId = questionId;
		}

		public String getUserAnswer() {
			return userAnswer;
		}

		public void setUserAnswer(String userAnswer) {
			this.userAnswer = userAnswer;
		}

	}

}
