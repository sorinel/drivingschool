package com.adisorin.drivingschool;

import android.app.Activity;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;

public class HomeActivity extends Activity {

	private Button startTestButton;
	private Button chooseCategoryButton;
	private static SQLiteDatabase database;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home);
		startTestButton = (Button) findViewById(R.id.startAppButton);
		chooseCategoryButton = (Button) findViewById(R.id.chooseCategoryButton);
		DBHelper dbHelper = new DBHelper(this);
		database = dbHelper.getWritableDatabase();
	}

	@Override
	protected void onStart() {
	       Log.d("TAG","--------onStart-----------");
		super.onStart();

		startTestButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivity(new Intent(HomeActivity.this, DrivingTestActivity.class));
				//first time starting test
				DrivingTestActivity.mIsFirstEntry = true;
			}
		});

		chooseCategoryButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivity(new Intent(HomeActivity.this, ChooseCategoryActivity.class));
			}
		});
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.home, menu);
		return true;
	}

	static SQLiteDatabase getDatabase() {
		return database;
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		Log.d("TAG","--------onDestroy-----------");
		database.close();
	}	

}
